import time
import datetime
from time import gmtime, strftime
import board
from busio import I2C
import adafruit_bme680
import serial
import pymysql
#import pypyodbc  
import signal
#import skywriter
  
# Create library object using our Bus I2C port
i2c = I2C(board.SCL, board.SDA)
bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c, debug=False)
## change this to match the location's pressure (hPa) at sea level
bme680.sea_level_pressure = 1013.25
#
ser = serial.Serial('/dev/ttyUSB0')
#TEST AWS
host="coucoudb3.c2yeqrr5ojgm.us-east-2.rds.amazonaws.com"
port=3306
dbname="coucouDB"
user="root"
password="Aze159?,?"
connection = pymysql.connect(host, user=user,port=port,
                           passwd=password, db=dbname)

liste_temperature = []
liste_gasres = []
liste_humidity = []
liste_pressure = []
liste_altitude = []
liste_pm10 = []
liste_pm25 = []
n =5
i=0
today = datetime.datetime.now().time()
today = strftime("%Y-%m-%d %H:%M:%S", gmtime())
print("INSERT")

try:
    #with connection.cursor() as mycursor:
        # Create a new record
        #mycursor.execute("CREATE TABLE table_donnee_raspberry_v3 (timeStart text, temperature text, gasres text, humidity text, pressure text, altitude text, pm25 text, pm10 text)")
        #connection.commit()
        #mycursor.execute("TRUNCATE TABLE table_donnee_raspberry_v3")
        #connection.commit()
 
    while i<n:
        i=i+1
        temperature = bme680.temperature
        gasres = bme680.gas
        humidity = bme680.humidity
        pressure = bme680.pressure
        altitude = bme680.altitude
        data = []
        for index in range(0,10):
            datum = ser.read()
            data.append(datum)
        pm25 = int.from_bytes(b''.join(data[2:4]), byteorder='little')/10
        pm10 = int.from_bytes(b''.join(data[4:6]), byteorder='little')/10
        
        liste_temperature.append(temperature)
        liste_gasres.append(gasres)
        liste_humidity.append(humidity)
        liste_pressure.append(pressure)
        liste_altitude.append(altitude)
        liste_pm10.append(pm10)
        liste_pm25.append(pm25)
        
        liste_temperature2 = str(liste_temperature)
        liste_gasres2 = str(liste_gasres)
        liste_humidity2 = str(liste_humidity)
        liste_pressure2 = str(liste_pressure)
        liste_altitude2 = str(liste_altitude)
        liste_pm102 = str(liste_pm25)
        liste_pm252 = str(liste_pm10)
        
        
        
        
        with connection.cursor() as cursor:
            sql = "INSERT INTO `table_donnee_raspberry_v3` (`timeStart`, `temperature`, `gasres`, `humidity`, `pressure`, `altitude`, `pm25`, `pm10`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(sql, (today,liste_temperature2,liste_gasres2,liste_humidity2,liste_pressure2,liste_altitude2,liste_pm102,liste_pm252))
            connection.commit()
            
            
            
        time.sleep(20)
        
    with connection.cursor() as cursor:
        sql = "SELECT * FROM table_donnee_raspberry_v3 ORDER BY timeStart DESC LIMIT 1"
        cursor.execute(sql)
        user1 = cursor.fetchall()
        print(user1)
finally:
    connection.close()

print("INSERT")


#
## Skywrtier section (comment this section if the script crashes)
#some_value = 5000
#@skywriter.flick()
#def flick(start,finish):
#    print('Got a flick!', start, finish)
#
#@skywriter.airwheel()
#def spinny(delta):
#    global some_value
#    some_value += delta
#    if some_value < 0:
#        some_value = 0
#    if some_value > 10000:
#        some_value = 10000
#    print('Airwheel:', some_value/100)
#
#@skywriter.double_tap()
#def doubletap(position):
#    print('Double tap!', position)
#
#@skywriter.tap()
#def tap(position):
#    print('Tap!', position)

#skywriter.touch()
#def touch(position):
#    print('Touch!', position)
## End of Skywriter section

# IoT_Ecologie
# API IoT
=====
1. Cloner le projet en utilisant la commande `git clone https://gitlab.com/adrien.corsaut/...`

2. Importer le projet dans IntelliJ IDEA en important le fichier "pom.xml" à la racine de ce repository.

3. Run "Application.java"

4. Ouvrir l'URL "http://localhost:8555/"
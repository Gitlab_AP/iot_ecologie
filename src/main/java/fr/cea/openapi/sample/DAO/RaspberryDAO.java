package fr.cea.openapi.sample.DAO;

import fr.cea.openapi.sample.model.RaspberryData;
import fr.cea.openapi.sample.model.RaspberryDataList;
import org.apache.catalina.filters.ExpiresFilter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RaspberryDAO {
    private static Connection con;
    private static RaspberryDAO raspberryDAOInstance = null;

    private RaspberryDAO(Connection con) {
        this.con = con;
    }

    public static RaspberryDAO getRaspberryDAO(Connection con) throws SQLException {
        if (raspberryDAOInstance == null) {
            raspberryDAOInstance = new RaspberryDAO(con);
            RaspberryDAO.con = con;
            createAllRaspberryData();
        }
        else
            RaspberryDAO.con = con;
            createAllRaspberryData();
        System.out.println("RaspberryDAO constructor : Refresh !");
        return raspberryDAOInstance;
    }

    public static void concatenerBis(ResultSet result, String label, ArrayList<RaspberryData >raspberryDataArrayList) throws SQLException {
        String id = "";
        ArrayList<String> id2 = new ArrayList<>();
        while (result.next()) {
            id = result.getString(label);
            id.replace("[", "");
            id.replace("]", "");
            id2.add(Arrays.asList(id.split(", ")).get(0));
        }
        for(int i=0;i<id2.size()-1;i++){
            if(id2.get(i).equals(id2.get(i+1))){

            }else{
                raspberryDataArrayList.add(new RaspberryData());
                raspberryDataArrayList.get(raspberryDataArrayList.size()-1).setTimeStart(id2.get(i));
            }
        }
        System.err.println(raspberryDataArrayList.get(raspberryDataArrayList.size()-1).getTimeStart());
    }
    public static void concatener(ResultSet result, String label, ArrayList<RaspberryData >raspberryDataArrayList) throws SQLException {
        ArrayList<ArrayList<Float>> idList = new ArrayList<>();
        ArrayList<List<String>> id4 = new ArrayList<>();
        ArrayList<Float> id3 = new ArrayList<>();
        String id="";
        String id2="";
        String sid3="";
        float temp=0;
        while (result.next()) {
                id= result.getString(label);
                id2 = id.replace("[" , "");
                sid3 = id2.replaceAll("]" , "");
                id4.add(Arrays.asList(sid3.split(", ")));



                if(id3.size()==id4.get(0).size()-1){
                    id3.add(Float.parseFloat(id4.get(0).get(id4.get(0).size()-1)));
                }
                if(id3.size()>id4.get(0).size()){
                    idList.add(new ArrayList<Float>());
                    for(int i =0 ; i<id3.size();i++){
                        temp = id3.get(i);
                        idList.get(idList.size()-1).add(temp);
                    }
                    id3.clear();
                    id3.add(Float.parseFloat(id4.get(0).get(id4.get(0).size()-1)));
                }
                id4.clear();
        }






        for(int i=0;i<idList.size();i++){
            if(label.equals("temperature")){


                raspberryDataArrayList.get(i).setTemperature(idList.get(i));
                for(int k = 0;k<raspberryDataArrayList.size();k++){
                    if(i==idList.size()-k){
                        raspberryDataArrayList.get(raspberryDataArrayList.size()-k).setTemperature(idList.get(i));
                     }
                }


            }
            else if(label.equals("gasres")){
                raspberryDataArrayList.get(i).setGasres(idList.get(i));
                for(int k = 0;k<raspberryDataArrayList.size();k++){
                    if(i==idList.size()-k){
                        raspberryDataArrayList.get(raspberryDataArrayList.size()-k).setGasres(idList.get(i));
                    }
                }
            }
            else if(label.equals("altitude")){
                raspberryDataArrayList.get(i).setAltitude(idList.get(i));
                for(int k = 0;k<raspberryDataArrayList.size();k++){
                    if(i==idList.size()-k){
                        raspberryDataArrayList.get(raspberryDataArrayList.size()-k).setAltitude(idList.get(i));
                    }
                }
            }
            else if(label.equals("pressure")){
                raspberryDataArrayList.get(i).setPression(idList.get(i));
                for(int k = 0;k<raspberryDataArrayList.size();k++){
                    if(i==idList.size()-k){
                        raspberryDataArrayList.get(raspberryDataArrayList.size()-k).setPression(idList.get(i));
                    }
                }
            }
            else if(label.equals("humidity")){
                raspberryDataArrayList.get(i).setHumidite(idList.get(i));
                for(int k = 0;k<raspberryDataArrayList.size();k++){
                    if(i==idList.size()-k){
                        raspberryDataArrayList.get(raspberryDataArrayList.size()-k).setHumidite(idList.get(i));
                    }
                }
            }
            else if(label.equals("pm10")){
                raspberryDataArrayList.get(i).setPm10(idList.get(i));
                for(int k = 0;k<raspberryDataArrayList.size();k++){
                    if(i==idList.size()-k){
                        raspberryDataArrayList.get(raspberryDataArrayList.size()-k).setPm10(idList.get(i));
                    }
                }
            }
            else if(label.equals("pm25")){
                raspberryDataArrayList.get(i).setPm25(idList.get(i));
                for(int k = 0;k<raspberryDataArrayList.size();k++){
                    if(i==idList.size()-k){
                        raspberryDataArrayList.get(raspberryDataArrayList.size()-k).setPm25(idList.get(i));
                    }
                }
            }
        }
    }
       //Prends toutes les données de la rapsberry => foctionne nickel
    public static void createAllRaspberryData() {
        System.out.println("RaspberryDAO createAllRaspberryData : Refresh !");
        ArrayList<RaspberryData> raspberryDataArrayList = new ArrayList<RaspberryData>();
        try {
            ResultSet result = con.createStatement().executeQuery("SELECT * FROM table_donnee_raspberry_v3" );
            /*while(result.next()){
                System.err.println("\n\n\n");
                System.err.println(result.getString(1));
                System.err.println(result.getString(2));
                System.err.println(result.getString(3));
                System.err.println(result.getString(4));
                System.err.println(result.getString(5));
                System.err.println(result.getString(6));

            }*/

            if (result.next()) {
                //result = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM table_donnee_raspberry" );

                result = con.createStatement().executeQuery("SELECT * FROM table_donnee_raspberry_v3" );
                concatenerBis(result, "timeStart",raspberryDataArrayList);
                result = con.createStatement().executeQuery("SELECT * FROM table_donnee_raspberry_v3" );
                concatener(result, "temperature",raspberryDataArrayList);
                result = con.createStatement().executeQuery("SELECT * FROM table_donnee_raspberry_v3" );
                concatener(result, "gasres",raspberryDataArrayList);
                result = con.createStatement().executeQuery("SELECT * FROM table_donnee_raspberry_v3" );
                concatener(result, "humidity",raspberryDataArrayList);
                result = con.createStatement().executeQuery("SELECT * FROM table_donnee_raspberry_v3" );
                concatener(result, "pressure",raspberryDataArrayList);
                result = con.createStatement().executeQuery("SELECT * FROM table_donnee_raspberry_v3" );
                concatener(result, "altitude",raspberryDataArrayList);
                result = con.createStatement().executeQuery("SELECT * FROM table_donnee_raspberry_v3" );
                concatener(result, "pm25",raspberryDataArrayList);
                result = con.createStatement().executeQuery("SELECT * FROM table_donnee_raspberry_v3" );
                concatener(result, "pm10",raspberryDataArrayList);

                RaspberryDataList.setRaspberryDataList(raspberryDataArrayList);
                System.out.println(raspberryDataArrayList.get(22));
                System.out.println(RaspberryDataList.getRaspberryDataList().get(1).getTemperature());
            } else
                System.err.println("Pas de raspberryData avec cet ID !");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package fr.cea.openapi.sample;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import fr.cea.openapi.sample.controller.LibraryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableSwagger2
public class Application {

    @Value("${security.activation.status}")
    private boolean securityActivationStatus;

    /**
     * Using this Postconstruct For cannot deserialized Datetime Format
     */
    @Autowired
    private ObjectMapper objectMapper;

    @PostConstruct
    public void setUp() {
        objectMapper.registerModule(new JavaTimeModule());
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        //create a libraryController to import data
        LibraryController libraryController = new LibraryController();
        //Launch the App
        SpringApplication.run(Application.class, args);

        //Refresh the data
        while(true) {
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("App Refresh !!!");
            libraryController.updateJavaObjectFromBDD();
        }
    }

    private final String filterPatern = "/openapi/.*";
    private final String basePackage = "fr.cea.openapi.sample.api";

    /**
     * the metadata are information visualized in the /basepath/swagger-ui.html
     * interface, only for documentation
     */
    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("Transfer-Service API documentation")
                .description("This is API documentation for working with Product Manager Engine")
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .termsOfServiceUrl("")
                .version("1.0.0")
                .contact(new Contact("", "", "thanhhai.nguyen@cea.fr"))
                .build();
    }

    /**
     * Docket is a SwaggerUI configuration component, in particular specifies to
     * use the V2.0 (SWAGGER_2) of swagger generated interfaces it also tells to
     * include only paths that are under /v1/. If other rest interfaces are
     * added with different base path, they won't be included this path selector
     * can be removed if all interfaces should be documented.
     */
    @Bean
    public Docket documentation() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        docket.apiInfo(metadata());
        if (!securityActivationStatus) {
            return docket
                    .select()
                    .apis(RequestHandlerSelectors.basePackage(basePackage))
                    .paths(PathSelectors.regex(filterPatern))
                    .build();
        } else {
            return docket
                    .securitySchemes(new ArrayList<BasicAuth>(Arrays.asList(new BasicAuth("basicAuth"))))
                    .securityContexts(new ArrayList<SecurityContext>(Arrays.asList(securityContext())))
                    .select()
                    .apis(RequestHandlerSelectors.basePackage(basePackage))
                    .paths(PathSelectors.regex(filterPatern))
                    .build()
                    ;
        }
    }

    /**
     * Selector for the paths this security context applies to ("filterPatern)
     */
    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth()).forPaths(PathSelectors.regex(filterPatern))
                .build();
    }

    /**
     * Here we use the same key defined in the security scheme (basicAuth)
     */
    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return new ArrayList<SecurityReference>(Arrays.asList(new SecurityReference("basicAuth", authorizationScopes)));
    }

}
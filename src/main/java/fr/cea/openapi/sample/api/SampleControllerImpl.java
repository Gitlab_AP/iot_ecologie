package fr.cea.openapi.sample.api;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import fr.cea.openapi.sample.model.RaspberryData;
import fr.cea.openapi.sample.model.RaspberryDataList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;

import fr.cea.openapi.sample.service.SampleService;

@Controller
public class SampleControllerImpl implements SampleController {

    @Autowired
    SampleService sampleService;

    protected Logger logger = Logger.getLogger(SampleControllerImpl.class.getName());

    //CREATION DE METHODES PERSO


    @Override
    public ResponseEntity<ArrayList<RaspberryData>> findRaspberryDataById() {
       // logger.info(String.format("Sample.findById(%s)"));


        return new ResponseEntity<ArrayList<RaspberryData>>( sampleService.findRaspberryDataById(), HttpStatus.OK);
    }


}

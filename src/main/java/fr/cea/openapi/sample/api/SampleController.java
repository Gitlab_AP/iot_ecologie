package fr.cea.openapi.sample.api;

import java.util.ArrayList;
import java.util.List;

import fr.cea.openapi.sample.model.RaspberryData;
import fr.cea.openapi.sample.model.RaspberryDataList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RequestMapping("/openapi")
public interface SampleController {

    final Logger logger = LoggerFactory.getLogger(SampleController.class.getName());

    //CREATION DE METHODES PERSO
    //Méthodes pour envoyer les données
    

    //Envoie les data rapsberry d'un patient en fonction de son ID
    @ApiOperation(value = "",
            nickname = "dataRaspberryOnePatient", notes = "call findRaspberryDataById",
            response = String.class,
            authorizations = {
            }, tags = {"Raspberry"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Server response", response = String.class),
            @ApiResponse(code = 400, message = "Bad request", response = String.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),
            @ApiResponse(code = 404, message = "Not found", response = String.class),
            @ApiResponse(code = 500, message = "Error for HTTPS call trustAnchors", response = String.class)})
    @RequestMapping(value = "/raspberry", produces = {"application/json"}, method = RequestMethod.GET)
    ResponseEntity<ArrayList<RaspberryData>> findRaspberryDataById();
}

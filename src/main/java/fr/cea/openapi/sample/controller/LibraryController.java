package fr.cea.openapi.sample.controller;

import fr.cea.openapi.sample.DAO.RaspberryDAO;
import fr.cea.openapi.sample.model.RaspberryDataList;

import java.sql.*;

import static java.sql.DriverManager.getConnection;

public class LibraryController {
    private static Connection conCoucouDB;


    public LibraryController() throws ClassNotFoundException, SQLException {
        updateJavaObjectFromBDD();
    }

    public void updateJavaObjectFromBDD() throws ClassNotFoundException, SQLException {

        //coucouDB
        Class.forName("com.mysql.jdbc.Driver");
        String myUrlCoucouDB = "jdbc:mysql://coucoudb3.c2yeqrr5ojgm.us-east-2.rds.amazonaws.com:3306/coucouDB?user=root&password=Aze159?,?"; //sur raspi : ifconfig et regarder "wlan0"
        conCoucouDB = DriverManager.getConnection(myUrlCoucouDB);
        System.out.println("LibraryController Refresh !!!");
        //Call the DAO
        //RaspberryDataList.clearRaspberryDataList();
        RaspberryDAO.getRaspberryDAO(conCoucouDB);
    }


    public static Connection getConCoucouDB() {
        return conCoucouDB;
    }

    public static void setConCoucouDB(Connection conCoucouDB) {
        LibraryController.conCoucouDB = conCoucouDB;
    }
}

package fr.cea.openapi.sample.model;

import java.util.ArrayList;

public class RaspberryDataList {

    //Attributs
    private static ArrayList<RaspberryData> raspberryDataList = new ArrayList<RaspberryData>();
    private static int currentId = 0;

    //Méthodes de la classe
    public static ArrayList<RaspberryData> getRaspberryDataList() {
        return raspberryDataList;
    }

    public static void setRaspberryDataList(ArrayList<RaspberryData> raspberryDataList) {
        RaspberryDataList.raspberryDataList = raspberryDataList;
    }


    public static int getCurrentId() {
        return currentId;
    }

    public static void setCurrentId(int currentId) {
        RaspberryDataList.currentId = currentId;
    }

    public static void addRaspberryData(RaspberryData r) {
        for(int i=0;i<raspberryDataList.size();i++)
        {
            if(r.getTimeStart().equals(raspberryDataList.get(i).getTimeStart())){
                raspberryDataList.remove(i);
            }
        }
        raspberryDataList.add(r);
    }


    public static void printList() {
        for (RaspberryData r : raspberryDataList) {
            System.out.println(r);
        }
    }

    /*public static void clearRaspberryDataList() {
        System.out.println("Before clear : "+raspberryDataList);
        raspberryDataList.clear();
        System.out.println("After clear : "+raspberryDataList);
    }
*/
    public static ArrayList<RaspberryData> findRaspberryDataById() {


        return raspberryDataList;
    }

}

package fr.cea.openapi.sample.model;

import java.util.ArrayList;

public class RaspberryData {

    private String timeStart;
    private ArrayList temperature;
    private ArrayList pm10; //Nouveau-né : 35 à 50 - 2 ans : 25 à 35 - 12 ans : 15 à 25 - Adulte : 14 à 20 -Personne âgée : 15 à 25
    private ArrayList pm25; //Nouveau-né : 35 à 50 - 2 ans : 25 à 35 - 12 ans : 15 à 25 - Adulte : 14 à 20 -Personne âgée : 15 à 25
    private ArrayList altitude; //Nouveau-né : 35 à 50 - 2 ans : 25 à 35 - 12 ans : 15 à 25 - Adulte : 14 à 20 -Personne âgée : 15 à 25
    private ArrayList humidite; //Nouveau-né : 35 à 50 - 2 ans : 25 à 35 - 12 ans : 15 à 25 - Adulte : 14 à 20 -Personne âgée : 15 à 25
    private ArrayList pression; //Nouveau-né : 35 à 50 - 2 ans : 25 à 35 - 12 ans : 15 à 25 - Adulte : 14 à 20 -Personne âgée : 15 à 25
    private ArrayList gasres; //Nouveau-né : 35 à 50 - 2 ans : 25 à 35 - 12 ans : 15 à 25 - Adulte : 14 à 20 -Personne âgée : 15 à 25

    public RaspberryData(String timeStart, ArrayList temperature, ArrayList pm10, ArrayList pm25, ArrayList altitude, ArrayList humidite, ArrayList pression, ArrayList gasres) {
        this.timeStart = timeStart;
        this.temperature = temperature;
        this.pm10 = pm10;
        this.pm25 = pm25;
        this.altitude = altitude;
        this.humidite = humidite;
        this.pression = pression;
        this.gasres = gasres;
    }

    public RaspberryData() {
    }

    public ArrayList getTemperature() {
        return temperature;
    }


    public ArrayList getPm10() {
        return pm10;
    }

    public void setPm10(ArrayList pm10) {
        this.pm10 = pm10;
    }

    public ArrayList getPm25() {
        return pm25;
    }

    public void setPm25(ArrayList pm25) {
        this.pm25 = pm25;
    }

    public ArrayList getAltitude() {
        return altitude;
    }

    public void setAltitude(ArrayList altitude) {
        this.altitude = altitude;
    }

    public ArrayList getHumidite() {
        return humidite;
    }

    public void setHumidite(ArrayList humidite) {
        this.humidite = humidite;
    }

    public ArrayList getPression() {
        return pression;
    }

    public void setPression(ArrayList pression) {
        this.pression = pression;
    }

    public ArrayList getGasres() {
        return gasres;
    }

    @Override
    public String toString() {
        return "RaspberryData{" +
                "timeStart='" + timeStart + '\'' +
                ", temperature=" + temperature +
                ", pm10=" + pm10 +
                ", pm25=" + pm25 +
                ", altitude=" + altitude +
                ", humidite=" + humidite +
                ", pression=" + pression +
                ", gasres=" + gasres +
                '}';
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public void setTemperature(ArrayList temperature) {
        this.temperature = temperature;
    }

    public void setGasres(ArrayList gasres) {
        this.gasres = gasres;
    }
}


package fr.cea.openapi.sample.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import fr.cea.openapi.sample.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SampleService {


    protected Logger logger = Logger.getLogger(SampleService.class.getName());
    private String appName; //a way to get the default parameter from application.yml

    //CREATION DE METHODES PERSO


    public ArrayList<RaspberryData> findRaspberryDataById() {
        ArrayList<RaspberryData> raspberryDataArrayList = RaspberryDataList.findRaspberryDataById();
        System.out.println("Raspberry Data List = "+raspberryDataArrayList);
        return raspberryDataArrayList;
    }
}

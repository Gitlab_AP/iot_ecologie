package fr.cea.openapi.sample.repository.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.cea.openapi.sample.Application;
import fr.cea.openapi.sample.api.SampleControllerImpl;

@ContextConfiguration(classes=Application.class)
@WebMvcTest(SampleControllerImpl.class)
@AutoConfigureMockMvc
@ActiveProfiles("test") // load application-test.properties
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SampleRepositoryTest {

	
	final static Logger logger = LoggerFactory.getLogger( UnitTest.class );
	
	@Test
    public void test01get() throws Exception {
        
		logger.info("Testing 1");
       
      // new EventHandlerNotifyService(callSubcribeEventHandler, callNotifyBundleManager).SubscribeEventHandler(); 
    }
	
	@Test
	public void testAddAccount() {
		logger.info("Testing logger.info");

//		Account a = new Account();
//		a.setNumber("12345678909");
//		a.setBalance(1232);
//		a.setCustomerId("234353464576586464");
//		repository.save(a);
	}
	
//	@Test
//  public void test02get() throws Exception {
//      
//      String param = "checker";
//      //expected output structure = "{\"name\":\"aName\",\"id\":\"anIdValue\",\"path\":\"aPath\",\"version\":\"1\"}";
//      
//      System.out.println("Testing get DSA ID"); 
//		this.mockMvc.perform(
//    		MockMvcRequestBuilders.fileUpload(endPoint)
//            .file(firstFile)
//            .file(secondFile)
//            .param("dsaId", "DSA-2d072942-9ed8-4258-95c2-da8c4a1a2e9a")
//            .param("hashCode", "1")
//                .accept(MediaType.APPLICATION_JSON)
//             )
//            .andDo(MockMvcResultHandlers.print())
//            .andExpect(status().isOk())
//            ;//  }
	
}

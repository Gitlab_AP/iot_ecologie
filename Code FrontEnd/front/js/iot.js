var session = 0;
var timestampSession = 1200;

var ctxHumidite = document.getElementById('myChartHumidite').getContext('2d');
var ctxTemperature = document.getElementById('myChartTemperature').getContext('2d');
var ctxPF1 = document.getElementById('myChartParticuleFine1').getContext('2d');
var ctxPF2 = document.getElementById('myChartParticuleFine2').getContext('2d');
var ctxPr = document.getElementById('myChartPression').getContext('2d');
var ctxA = document.getElementById('myChartAltitude').getContext('2d');
var ctxD = document.getElementById('myChartGasRes').getContext('2d');


var configHumidite = {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [],
   		datasets: [{
	       backgroundColor: 'rgb(132, 99, 255, 0.25)',
            borderColor: 'rgb(132, 99, 255)',
            data: []
        }]
    },

    // Configuration options go here
    options: {
    	legend: {
            display: false
        },
        title: {
            display: true,
            text: "Taux d'humidité relative"
        },
        hover: {
			mode: 'nearest',
			intersect: true
		},
		scales: {
            yAxes: [{
                scaleLabel: {
        			display: true,
        			labelString: '% Humidité relative'
      			}
            }],
            xAxes: [{
            	scaleLabel: {
        		    display: true,
    			    labelString: 'Temps'
      			}
            }]
        },
		maintainAspectRatio: false
    }
};

var configTemperature = {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [],
        datasets: [{
           backgroundColor: 'rgb(132, 255, 255, 0.25)',
            borderColor: 'rgb(132, 255, 255)',
            data: []
        }]
    },

    // Configuration options go here
    options: {
        legend: {
            display: false
        },
        title: {
            display: true,
            text: 'Température'
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: '°C'
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Temps'
                }
            }]
        },
        maintainAspectRatio: false
    }
};

var configPF1 = {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [],
        datasets: [{
           backgroundColor: 'rgb(255, 99, 132, 0.25)',
            borderColor: 'rgb(255, 99, 132)',
            data: []
        }]
    },

    // Configuration options go here
    options: {
        legend: {
            display: false
        },
        title: {
            display: true,
            text: 'Particules ultrafines'
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'ng/m3'
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Temps'
                }
            }]
        },
        maintainAspectRatio: false
    }
};

var configPF2 = {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [],
        datasets: [{
           backgroundColor: 'rgb(99, 255, 132, 0.25)',
            borderColor: 'rgb(99, 255, 132)',
            data: []
        }]
    },

    // Configuration options go here
    options: {
        legend: {
            display: false
        },
        title: {
            display: true,
            text: 'Particules fines'
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'ng/m3'
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Temps'
                }
            }]
        },
        maintainAspectRatio: false
    }
};

var configPr = {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [],
        datasets: [{
           backgroundColor: 'rgb(255, 255, 0, 0.25)',
            borderColor: 'rgb(255, 255, 0)',
            data: []
        }]
    },

    // Configuration options go here
    options: {
        legend: {
            display: false
        },
        title: {
            display: true,
            text: "Pression de l'air"
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Pa'
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Temps'
                }
            }]
        },
        maintainAspectRatio: false
    }
};

var configA = {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [],
        datasets: [{
           backgroundColor: 'rgb(255, 192, 203, 0.25)',
            borderColor: 'rgb(255, 192, 203)',
            data: []
        }]
    },

    // Configuration options go here
    options: {
        legend: {
            display: false
        },
        title: {
            display: true,
            text: 'Altitude du capteur'
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Mètres'
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Temps'
                }
            }]
        },
        maintainAspectRatio: false
    }
};

var configD = {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [],
        datasets: [{
           backgroundColor: 'rgb(88, 41, 0, 0.25)',
            borderColor: 'rgb(88, 41, 0)',
            data: []
        }]
    },

    // Configuration options go here
    options: {
        legend: {
            display: false
        },
        title: {
            display: true,
            text: "Résistance de l'air"
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Ohm'
                }
            }],
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Temps'
                }
            }]
        },
        maintainAspectRatio: false
    }
};


var chartHumidite = new Chart(ctxHumidite, configHumidite);
var chartTemperature = new Chart(ctxTemperature, configTemperature);
var chartPF1 = new Chart(ctxPF1, configPF1);
var chartPF2 = new Chart(ctxPF2, configPF2);
var chartPr = new Chart(ctxPr, configPr);
var chartA = new Chart(ctxA, configA);
var chartD = new Chart(ctxD, configD);

function removeAllData(chart) {
    while(chart.data.labels.length != 0){
        chart.data.labels.pop();
        chart.data.datasets.forEach((dataset) => {
            dataset.data.pop();
        });
    }
    
    chart.update();
}

function addData(chart, timestamp, data, timeStart) {

    var hours = parseInt(timeStart.substring(0, 2));
    var minutes = parseInt(timeStart.substring(3, 5));
    var seconds = parseInt(timeStart.substring(6, 8)) + timestamp;

    while(seconds>=60){
        seconds = seconds - 60;
        minutes++;
    }

    while(minutes>=60){
        minutes = minutes - 60;
        hours++;
    }

    while(hours>=24){
        hours = hours - 24;
    }

    var time = getPartOfTimeFormat(hours) + "" + getPartOfTimeFormat(minutes) + "" + getPartOfTimeFormat(seconds);


    chart.data.labels.push(getTimeFormat(time));
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

function getJSON(url, callback){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    
    xhr.onload = function() {
    
        var status = xhr.status;
    
        if (status == 200) {
            callback(null, xhr.response);
        } else {
            callback(status);
        }
    };

    xhr.send();
}

function getTimeFormat(time){
    var t = "" + time;

    if(t.length === 4){
        t = "00" + t;
    }

    if(t.length === 5){
        t = "0" + t;
    }

    return t = t.substring(0, 2) + ":" + t.substring(2, 4) + ":" + t.substring(4, 6);
}

function getPartOfTimeFormat(partoftime){
    var t = "" + partoftime;

    if(t.length === 0){
        t = "00" + t;
    }

    if(t.length === 1){
        t = "0" + t;
    }

    return t;
}

function changeSession(i){
    session = i;

    getJSON('http://localhost:8555/openapi/raspberry',  function(err, data1) {  // https://jsonblob.com/api/jsonBlob/aa6942c7-3675-11ea-a549-8d642d393327 pour tester
    
        if (err != null) {
            console.error(err);
        } else {      

            removeAllData(chartHumidite);
            removeAllData(chartTemperature);
            removeAllData(chartPF1);
            removeAllData(chartPF2);
            removeAllData(chartPr);
            removeAllData(chartA);
            removeAllData(chartD);

            $(".menu-side").empty();

            for(i in data1){
                $(".menu-side").append("<p onClick=changeSession(" + i + ")>" + data1[i].timeStart + "</p>");
            }

            

            for(i in data1[session].humidite){
                addData(chartHumidite, timestampSession*i, data1[session].humidite[i], data1[session].timeStart.substring(11,19));
            }

            for(i in data1[session].temperature){
                addData(chartTemperature, timestampSession*i, data1[session].temperature[i], data1[session].timeStart.substring(11,19));
            }

            for(i in data1[session].pm25){
                addData(chartPF1, timestampSession*i, data1[session].pm25[i], data1[session].timeStart.substring(11,19));
            }

            for(i in data1[session].pm10){
                addData(chartPF2, timestampSession*i, data1[session].pm10[i], data1[session].timeStart.substring(11,19));
            }

            for(i in data1[session].pression){
                addData(chartPr, timestampSession*i, data1[session].pression[i]*100, data1[session].timeStart.substring(11,19));
            }

            for(i in data1[session].altitude){
                addData(chartA, timestampSession*i, -data1[session].altitude[i], data1[session].timeStart.substring(11,19));
            }

            for(i in data1[session].gasres){
                addData(chartD, timestampSession*i, data1[session].gasres[i], data1[session].timeStart.substring(11,19));
            }

            $("#titleH").html(data1[session].timeStart);
        }
    });
}


(function(){
    changeSession(session);
    setInterval(function myFunction() {
        changeSession(session)
    }, 15000);
})()
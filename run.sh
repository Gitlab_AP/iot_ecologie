#!/bin/sh

echo --------------------------------------------------------------------
echo Starting account service using the default parameter defined in application.yml
echo --------------------------------------------------------------------
mvn clean package && java -jar target/sample-service.jar
